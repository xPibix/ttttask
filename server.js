const path = require('path')
const { resolve } = require('path')
const fastifyPug = require('fastify-pug');
const homeRoute = require('./routes/home')
const fastify = require('fastify')({
    logger: false
})
fastify.register(require('fastify-formbody'))
fastify.register(require('fastify-multipart'))

fastify.register(require('fastify-static'), {
    root: path.join(__dirname, 'views'),
    dir: resolve(process.cwd(), 'routes')
})
fastify.register(fastifyPug, {views: 'views',
    fallbackViews: 'views',
    baseDir: path.join(__dirname, 'views')
    });


fastify.register(homeRoute)

fastify.listen(3000, function (err, address) {
    if (err) {
        process.exit(1)
    }
    console.log(`Server listening at ${address}`)
})


