const uuid = require('uuid').v4;
const fs = require("fs");
const path = require("path");


class User {
    constructor(login, password) {
        this.login = login
        this.password = password
        this.id = uuid()
    }

    toJSON() {
        return {
            login: this.login,
            password: this.password,
            id: this.id
        }
    }
    async save() {
        const users = await User.getAll()
        users.push(this.toJSON())

        return new Promise((resolve, reject) => {
            fs.writeFile(
                path.join(__dirname, '..', 'public', 'users.json'),
                JSON.stringify(users),
                (err) => {
                    if (err) {
                        reject(err)
                    } else {
                        resolve()
                    }
                }
            )
        })
    }
    static getAll() {
        return new Promise((resolve, reject) => {
            fs.readFile(
                path.join(__dirname, '..', 'public', 'users.json'),
                'utf-8',
                (err, content) => {
                    if (err) {
                        reject(err)
                    } else {
                        resolve(JSON.parse(content))
                    }
                }
            )
        })
    }
}

module.exports = User